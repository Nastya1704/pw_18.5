﻿
#include <iostream>
#include <string>
using namespace std;

class Game
{
private:
    std::string name;
    int score;
    
};

int main()
{
    std::cout << "Enter number of players:\n";
    int x;
    std::cin >> x;
    const int players = x;
    int* arrayScore = new int[players];
    string* arrayName = new string[players];
        
    std::string name;
    int score;
    for (int j = 0; j < players; j++)
    {        
        std::cout << "Enter player name:\n";        
        std::cin >> name;                
        arrayName[j] = name;

        std::cout << "Enter this player score:\n";
        std::cin >> score;
        arrayScore[j] = score;
    }      
    
    for (int i = 0; i < players; i++)
    {
        for (int j = 0; j < players; j++)
        {
            if (arrayScore[i] > arrayScore[j])
            {
                score = arrayScore[i];
                arrayScore[i] = arrayScore[j];
                arrayScore[j] = score;
                name = arrayName[i];
                arrayName[i] = arrayName[j];
                arrayName[j] = name;
            }
        }
    }        
    for (int i = 0; i < players; i++)
    {
           std::cout << arrayName[i] << " " << arrayScore[i] << "\n";
    }   
    
}

